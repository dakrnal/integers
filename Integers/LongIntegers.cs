﻿namespace Literals
{
    public static class LongIntegers
    {
        public static long ReturnLongInteger21()
        {
            return 4956185095298947214L;
        }

        public static long ReturnLongInteger22()
        {
            return -1280010762458239942L;
        }

        public static long ReturnLongInteger23()
        {
            return -945783496234828465L;
        }

        public static ulong ReturnLongInteger24()
        {
            return 16269823234523742845L;
        }

        public static long ReturnLongInteger25()
        {
            return 9223372036854775807L;
        }

        public static long ReturnLongInteger26()
        {
            return 773738404492802748L;
        }

        public static ulong ReturnLongInteger27()
        {
            return 17977307477258691517;
        }

        public static ulong ReturnLongInteger28()
        {
            return 14193065825095688383;
        }

        public static long ReturnLongInteger29()
        {
            return 4100761908933204629;
        }

        public static long ReturnLongInteger210()
        {
            return 1645102583813967509;
        }

        public static long ReturnLongInteger211()
        {
            return 6148914691236517205;
        }

        public static long ReturnLongInteger212()
        {
            return 8446744073709551615;
        }
    }
}
